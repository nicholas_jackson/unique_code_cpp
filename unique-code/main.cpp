//
//  main.cpp
//  unique-code
//
//  Created by nicholas jackson on 28/10/2013.
//  Copyright (c) 2013 nicholas jackson. All rights reserved.
//

#include <iostream>
#include <string>
#include <set>
#include <cmath>
#include <fstream>
#include <ctime>

using namespace std;

class unique_code
{
public:
    
    //constructor
    unique_code(size_t num_codes, size_t code_length, const string output_file = "", const string charset = "", const string & existing = "");
    
    //destructor
    ~unique_code(){};
    
    // main generation function
    bool generate();
    
private:
    
    // defined character set
    string charset;
    // defined output file
    string output_file;
    // set of unique codes
    set<string> m_codes;
    // existing codes
    set<string> m_existing_codes;
    // number of required codes
    size_t num_codes;
    // length of codes
    size_t code_length;
    
    
    // generate random code
    string generate_code(size_t length);
    // check all variables
    bool check_settings();
    // write codes to defined file.
    void write_file();
    
    bool load_existing_codes(const string & existing);
    
};

unique_code::unique_code(size_t num_codes, size_t code_length, const string output_file, const string charset, const string & existing)
{
    //check and write character set
    if(charset == "")
    {
        this->charset = "23456789ABCDEFGHJKLMNPQRSTUVWXYZ";
    }
    else
    {
        this->charset = charset;
    }
    
    //check and write output file
    if(output_file == "")
    {
        this->output_file = "out.csv";
    }
    else
    {
        this->output_file = output_file;
    }
    
    //set number of codes and code length
    this->num_codes = num_codes;
    this->code_length = code_length;
    
    if(existing != "")
    {
        if(this->load_existing_codes(existing))
            this->num_codes += m_existing_codes.size();
    }
    
    
}

string unique_code::generate_code(size_t length)
{
    string code = "";
    string charset = this->charset;
    
    for(size_t i = 0; i < length; ++i)
    {
        code += charset.substr((rand() % charset.length()), 1);
    }
    
    return code;
}

bool unique_code::generate()
{
    if(!check_settings())
    {
        cout << "error" << endl;
        return false;
    }
    
    while(m_codes.size() < num_codes)
    {
        m_codes.insert(generate_code(code_length));
    }
    
    write_file();
    
    return true;
}

bool unique_code::check_settings()
{
    if(charset == "")
    {
        cout << "charset error." << endl;
        return false;
    }
    
    if(output_file == "")
    {
        cout << "output file error." << endl;
        return false;
    }
    
    if(num_codes == 0)
    {
        cout << "num codes error." << endl;
        return false;
    }
    
    if(code_length == 0)
    {
        cout << "code length error." << endl;
        return false;
    }
    
    if(num_codes > pow((double)charset.size(), code_length))
    {
        cout << "Not enough possible combinations." << endl;
        return false;
    }
    
    return true;
}

void unique_code::write_file()
{
    fstream out;
    out.open(output_file.c_str(), fstream::out);
    set<string>::iterator iter;
    set<string>::iterator iter2;
    
    if(!out.is_open())
        cout << "could not open. \"" << output_file << "\"" << endl;
    
    for(iter = m_codes.begin(); iter != m_codes.end(); iter++)
    {
        iter2 = m_existing_codes.find(*iter);
        
        if(iter2 == m_existing_codes.end())
            out << "\"" << *iter << "\"" << endl;
    }
    
    out.close();
}

bool unique_code::load_existing_codes(const string & existing)
{
    ifstream in(existing);
    
    if(!in.is_open())
    {
        cout << "Failed to read existing codes" << endl;
        return false;
    }
    
    while(!in.eof())
    {
        string code = "";
        getline(in, code);
        
        if(code == "")
            continue;
        
        code = code.substr(1, code.length()-1);
        
        m_existing_codes.insert(code);
        m_codes.insert(code);
    }
    
    return true;
}

int main(int argc, const char * argv[])
{
    if(argc < 4)
    {
        cout << "Not enough arguments." << endl;
        return 1;
    }
    
    string existing = "";
    
    if(argc == 5)
        existing = string(argv[4]);
    
    unique_code code(atoi(argv[1]), atoi(argv[2]), string(argv[3]));

    // take time snapshot
    clock_t time = clock();
    // generate codes
    code.generate();
    // take time difference after calculation.
    time = clock() - time;
    
    cout << "Generated " << atoi(argv[1]) << " codes in " << ((double)time)/CLOCKS_PER_SEC << " seconds." << endl;
    
    return 0;
}

